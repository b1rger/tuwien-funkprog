import Angabe3 hiding (main)
import Test.Tasty
import Test.Tasty.HUnit

main = defaultMain spec

spec :: TestTree
spec = testGroup "fp_lu03_201029"
  [
    a1,
    a2,
    a3,
    a4,
    a5,
    a6,
    a7,
    a8,
    a9
  ]

a1 :: TestTree

a1 = testGroup "A1"
  [
     testCase "1" $ do show Ungueltig @?= "Kein Intervall",
     testCase "2" $ do show Leer  @?= "<>",
     testCase "3" $ do show (IV (2,5)) @?= "<2,5>",
     testCase "4" $ do show (IV (5,2)) @?=  "<>"
  ]


a2 :: TestTree

a2 = testGroup "A2"
  [
    testCase "1" $ do IV (2,5) == IV (2,5) @?= True,
    testCase "2" $ do IV (2,5) == IV (5,2) @?= False,
    testCase "3" $ do IV (2,5) /= IV (2,5) @?= False,
    testCase "4" $ do IV (5,2) == Leer @?= True,
    testCase "5" $ do IV (2,5) /= Ungueltig @?= error "Vergleich nicht moeglich"
  ]

a3 = testGroup "A3"
  [
    testCase "1" $ do IV (2,5) > IV (3,4) @?= True,
    testCase "2" $ do IV (2,5) >= IV (3,5) @?= True,
    testCase "3" $ do IV (2,5) < IV (1,6) @?= True,
    testCase "4" $ do IV (2,5) <= IV (1,5) @?= True,
    testCase "5" $ do IV (2,5) < Ungueltig @?= error "Vergleich nicht moeglich",
    testCase "6" $ do Ungueltig < Leer @?= error "Vergleich nicht moeglich",
    testCase "7" $ do Leer <= IV (1,5) @?= True
  ]

a4 = testGroup "A4"
  [
    testCase "1" $ do IV (2,5) + IV (3,4) @?= IV(5,9),
    testCase "2" $ do IV (3,7) - IV (1,2) @?= IV(1,6),
    testCase "3" $ do abs (IV (4,8)) @?= IV(4,8),
    testCase "4" $ do abs (IV (-4,7)) @?= IV(0,7),
    testCase "5" $ do abs (IV (4,-7)) @?= Leer 
  ]

a5 = testGroup "A5"
  [
    testCase "1" $ do fromEnum (IV(2,2)) @?= 2,
    testCase "2" $ do fromEnum (IV(2,3)) @?= error "Operation nicht moeglich",
    testCase "3" $ do fromEnum Ungueltig @?= error "Operation nicht moeglich",
    testCase "4" $ do ((toEnum 2) :: Intervall) @?= IV(2,2)
  ]

a6 = testGroup "A6"
  [
    testCase "1" $ do kanonisch (IV(2,3)) @?= IV(2,3),
    testCase "2" $ do kanonisch (IV(3,2)) @?= Leer
  ]

a7 = testGroup "A7"
  [
    testCase "1" $ do is_elem 3 (IV(3,5)) @?= Just True,
    testCase "2" $ do is_elem 23 (IV(3,5)) @?= Just False
  ]

a8 = testGroup "A8"
  [
   testCase "1" $ do codiere [] @?= Leer,
   testCase "2" $ do codiere [1..3] @?= IV(1,3),
   testCase "3" $ do codiere [2,3,4,5] @?= IV(2,5),
   testCase "4" $ do codiere [2,5,4] @?= Ungueltig,
   testCase "5" $ do codiere [3,2,5,4] @?= Ungueltig,
   testCase "6" $ do codiere [2,2,3,4,5] @?= Ungueltig,
   testCase "7" $ do codiere [5,4..2] @?= Ungueltig,
   testCase "8" $ do codiere [2..5] @?= IV(2,5),
   testCase "9" $ do decodiere Leer @?= Just [],
   testCase "10" $ do decodiere Ungueltig @?= Nothing,
   testCase "11" $ do decodiere (IV (2,5)) @?= Just [2,3,4,5],
   testCase "12" $ do decodiere (IV (5,2)) @?= Just []
  ]

a9 = testGroup "A9"
  [
    testCase "1" $ do extrahiere (Just ([2..5]::[Int])) @?= [2,3,4,5],
    testCase "2" $ do extrahiere (Just ([5..2]::[Int])) @?= [],
    testCase "3" $ do extrahiere (Just ([]::[Int])) @?= [],
    testCase "4" $ do extrahiere (Nothing::Maybe[Int]) @?= error "Extraktion nicht moeglich",
    testCase "5" $ do ist_aufsteigend ([2..5]::[Int]) @?= True,
    testCase "6" $ do ist_aufsteigend ([2,5..21]::[Int]) @?= True,
    testCase "7" $ do ist_aufsteigend ([5..2]::[Int]) @?= True,
    testCase "8" $ do ist_aufsteigend ([5,4..2]::[Int]) @?= False,
    testCase "9" $ do ist_lueckenlos ([2..5]::[Int]) @?= True,
    testCase "10" $ do ist_lueckenlos ([2,5..21]::[Int]) @?= False,
    testCase "11" $ do ist_lueckenlos ([5..2]::[Int]) @?= True,
    testCase "12" $ do ist_lueckenlos ([5,4,3,1,2]::[Int]) @?= True,
    testCase "13" $ do ist_lueckenlos ([5,3,4,5,1,3,5,2]::[Int]) @?= True,
    testCase "14" $ do ist_laL_Element 3 (Just ([2..5]::[Int])) @?= True,
    testCase "15" $ do ist_laL_Element 3 (Just ([5..2]::[Int])) @?= False,
    testCase "16" $ do ist_laL_Element 3 (Just ([]::[Int])) @?= False,
    testCase "17" $ do ist_laL_Element 3 (Just ([5,4..2]::[Int])) @?= False,
    testCase "18" $ do ist_laL_Element 8 (Just ([2,5..21]::[Int])) @?= False,
    testCase "19" $ do ist_laL_Element 5 (Just ([1,2,3,3,4,5,5,5]::[Int])) @?= True,
    testCase "20" $ do ist_laL_Element 3 (Nothing::Maybe [Int]) @?= False
  ]
